<?php
/**
 * OleExport index.php
 * @author fugu GmbH (fugu.ch)
 */
//error_reporting(0);

use ch\fugu\oledata\driver\AbstractOleDriver;
use ch\fugu\oledata\OleExport;

require_once('../ch/fugu/oledata/autoload.php');
$config = OleExport::getConfig('config.php');

$driverClassName = null;
if(!empty($config['driver'])){
    $driverClassName = $config['driver'];
}
else {
    $driverClassName = 'mydriver';
    require_once($driverClassName.'.php');
}

/**
 * @var AbstractOleDriver $driver
 */
$driver = new $driverClassName();
$driver->init($config);
$driver->execute();
